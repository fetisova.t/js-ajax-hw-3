/**
 * Created on 13.09.2019.
 */
const btnContact = document.querySelector('#contact-btn');
btnContact.addEventListener('click',(e)=>{
    document.cookie = "experiment=novalue; max-age=300";
    let cookieArr = document.cookie.split(';');
    let newCookieArr = [];
    cookieArr.forEach((el)=>{
       let arrItem = el.trim().split('=');
       newCookieArr.push(arrItem);
    });
    let cookieFlatArr = newCookieArr.flat(1);
    if(cookieFlatArr.includes("new-user")){
        document.cookie = "new-user=false";
    }else{
        document.cookie = "new-user=true";
    }
});